package Controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Communication.Client;

import java.io.*;

import javax.swing.JFileChooser;
import javax.swing.tree.DefaultMutableTreeNode;

import Entity.Message1;
import Entity.SysFile;
import Entity.User;
import View.AddFileUI;
import View.PrivateSpaceUserUI;

public class AddFileController implements Serializable {
	AddFileUI af;
	Client client;	
	User user;
	SysFile file;
	PrivateSpaceUserUI spacegui;
	PrivateSpaceController spacecontroller;
	File f1=null;
	String[] filename;
	String des;
	String location;

	AddFileController afc;



	public AddFileController (Client client)
	{
		this.client= client;
		afc=this;
		file=new SysFile();
		af= new AddFileUI();
		af.setVisible(true);
		af.addApplylActionListener(new ApplyListener());
		af.addBrouseActionListener(new BrowseFileListener());
		
	}
	class BrowseFileListener implements ActionListener
	{

		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fs=new JFileChooser(new File("c:\\"));
			fs.setDialogTitle("Upload a file");
			fs.setBackground(new Color(245, 255, 250));
			int result=fs.showOpenDialog(null);
			if (result==JFileChooser.APPROVE_OPTION)
			{
				f1 = fs.getSelectedFile();
				af.setLblFile(f1.getName());

			}


		}}
	class ApplyListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {

			location = " f";
			des=af.getTextArea();

			if(location.equals("") || des.equals("") || f1==null)
				SystemMessageController.displayErrorMessage("Please fill all fields");
			else
			{
				filename=f1.getName().split("\\.");	

				String[] str= des.split("\\s+");
				if(str.length>30)
					SystemMessageController.displayErrorMessage("Description must contain less than 30 words");
				else
				{
					Message1 m= new Message1(filename[0],"checkFileName");

					try {
						client.sendToServer(m);
						StartClientController.conClient.setCurrObj(getAfc());

					} catch (IOException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
				}
				
				
			}
		}
	}

	
	public void handleDBResult(Object msg)
	{
		
		if (msg.equals(""))
		{
			file.setF(f1);
			file.setFile_name(filename[0]);
			file.setDescription(des);
			file.setLocation(location);
			System.out.println(file.getF().getPath());
			
			try
			{
				File oldFile= new File(file.getF().getPath());
				byte [] bArr= new byte [(int)oldFile.length()];
				FileInputStream fis= new FileInputStream(oldFile);
				BufferedInputStream bis= new BufferedInputStream(fis);
				
				
				file.initArray(bArr.length);
				file.setSize(bArr.length);
				
				fis.read(bArr);
				fis.close();
				
				String str = file.getF().getName();
				Message1 m= new Message1(bArr,"save",str);
				client.sendToServer(m);
				Message1 m2= new Message1(file,"updatefile",User.Uname);
				client.sendToServer(m2);
							

			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(msg.equals("doneUpdate"))
		{
			af.dispose();
			PrivateSpaceController.fileList.add(filename[0]);
		}


	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public AddFileController getAfc() {
		return afc;
	}

	public void setAfc(AddFileController afc) {
		this.afc = afc;
	}
}
