package Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import java.io.IOException;
import java.io.Serializable;

import View.ClientUI;
import View.PrivateSpaceSysAdminUI;
import View.PrivateSpaceUserUI;
import Communication.Client;
import Entity.ClientModel;
import Entity.LoginMode;
import Entity.User;
import Entity.Message1;



public class ClientController
{
	User user;
	ClientUI clientui;
	ClientModel client;
	User log;
	ClientController temp;
	Client cc; 
	//PrivateSpaceSysAdminController sys;
	PrivateSpaceUserController normal;

	public ClientController getTemp() {
		return temp;
	}

	public void setTemp(ClientController temp) {
		this.temp = temp;
	}

	public ClientController(ClientUI clientui,ClientModel client,Client cc)
	{
		temp=this;

		this.clientui = clientui;
		this.client = client;
		this.cc=cc;
		clientui.addLoginActionListener(new LoginListener());
		this.clientui.setVisible(true);
	}

	public void handleDBResult (Object msg)
	{	
		user=(User)msg;
		if(user.getIsSysAdmin().equals("1"))
		{
			//sys= new PrivateSpaceSysAdminController();
			//sys.setVisible(true);
			clientui.dispose();
		}
		else
		{
			normal= new PrivateSpaceUserController(cc,temp,1);

			clientui.dispose();
		}

	}

	public User getLog() {
		return log;
	}

	public void setLog(User log) {
		this.log = log;
	}

	class LoginListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent w) {

			String pass=clientui.getPasstxt();
			String user = clientui.getUnametxt();

			if(pass.equals("")|| user.equals(""))
			{
				clientui.displayMsg("Please fills all fileds");
			}
			else
			{
				try
				{
					log=new User();
					log.setUserName(user);
					log.setPassword(pass);
					log.Uname=log.getUserName();
					Message1 me=new Message1(log,"searchLogin");
					cc.sendToServer(me);
					StartClientController.conClient.setCurrObj(getTemp());
				}catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
	}
}

