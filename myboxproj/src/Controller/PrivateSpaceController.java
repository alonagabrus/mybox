package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import View.ClientUI;
import View.ConnectToServerUI;
import View.PrivateSpaceSysAdminUI;
import View.PrivateSpaceUI;
import View.PrivateSpaceUserUI;
import Communication.Client;
import Entity.ClientModel;
import Entity.Message1;
import Entity.SysFile;
import Entity.User;

public class PrivateSpaceController {

	PrivateSpaceUI privateSpace;
	Client client;
	User user;
	PrivateSpaceController temp;
	ConnectToServerUI cts;
	public static ArrayList<String> fileList;

	
	public PrivateSpaceController getTemp() {
		return temp;
	}
	public void setTemp(PrivateSpaceController temp) {
		this.temp = temp;
	}
	ClientController cc;
	int type;
	

	public PrivateSpaceController(Client clien,ClientController clientcontroller,int type) {
		this.client=clien;
		if(type==1)
			privateSpace = new PrivateSpaceUserUI();
		else 
			privateSpace = new PrivateSpaceSysAdminUI();
		privateSpace.setVisible(true);
		cc=clientcontroller;
		user = cc.getLog();
		temp=this;
		fileList= new ArrayList<String>();
		privateSpace.setArray(fileList);
	//	privateSpace.addRemoveTypeActionListener(new RemoveListener());
		privateSpace.addLogoutActionListener(new LogOutListener());
	}
	class LogOutListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			try {
				Message1 m = new Message1(user,"logout");
				client.sendToServer(m);
				StartClientController.conClient.setCurrController(getTemp());
			} catch (IOException e1) {
				e1.printStackTrace();
			}			
		}
		}
	public void handleDBResult(String s)
	{
		if(s.equals("logout"))
		{
			
			ClientController gui = new ClientController(new ClientUI(), new ClientModel(), client);
			privateSpace.dispose();
		}
	}

	
	

	
		
		

		
	
		

}
