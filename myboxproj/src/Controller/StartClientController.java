package Controller;

import Communication.Client;
import Entity.ClientModel;
import View.ClientUI;
import View.ConnectToServerUI;


public class StartClientController {
	
	public static Client conClient;
	
	
	public static void open(Client c) {
		ClientUI clientui= new ClientUI();
		ClientModel client = new ClientModel();
		
		ClientController start = new ClientController(clientui,client,c);
	}

}
