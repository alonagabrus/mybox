package View;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;







import Communication.Client;
import Controller.ClientController;
import Controller.StartClientController;
import Controller.SystemMessageController;
import Entity.LoginMode;

import java.awt.event.ActionEvent;
import java.io.IOException;

public class ConnectToServerUI extends JFrame {

	final public static int DEFAULT_PORT = 5555;
	private String host;
	Client client; 
	ClientUI login;
	private JPanel contentPane;
	private JTextField textField; 
	JButton btnConnect;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConnectToServerUI connect = new ConnectToServerUI();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public ConnectToServerUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 150);
		setTitle("Connect to server");
		setResizable(false);
		setVisible(true);
		ImageIcon icon =new ImageIcon(getClass().getResource("/images/icon.png"));
		setIconImage(icon.getImage());
		contentPane = new JPanel();
		contentPane.setBackground(new Color(250, 240, 230));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		JLabel lblServerIp = new JLabel("Server IP:");
		lblServerIp.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblServerIp.setBounds(22, 39, 97, 19);
		contentPane.add(lblServerIp);

		textField = new JTextField();
		textField.setBounds(120, 40, 196, 19);
		textField.setText("10.10.3.63");
		contentPane.add(textField);
		textField.setColumns(10);

		btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try{
				if (textField.getText().equals(""))
				{
					displayWarningMessage("Please insert an IP address");
				}
				else
				{
					setHost(getTextField());
					StartClientController.conClient= new Client(getHost(),DEFAULT_PORT);
						if(StartClientController.conClient.isConnected())
						{
							dispose();
							ClientUI login= new ClientUI();
							StartClientController.open(StartClientController.conClient);
		
						}
						else
						{
							displayWarningMessage("Failed to connect. check IP");
							setTextField();
							
						}
				}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						displayWarningMessage("Connection was failed\n Probably IP is incorrect\n Try again");
						dispose();
					}					
				}
			
		});
		
		btnConnect.setBackground(new Color(245, 255, 250));
		btnConnect.setBounds(108, 76, 109, 23);
		contentPane.add(btnConnect);		
	}
	public void displayWarningMessage(String msg)
	{
		JOptionPane.showMessageDialog(this, msg);
	}
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	public String getTextField() {
		return textField.getText();
	}

	public JButton getBtnConnect() {
		return btnConnect;
	}

	public void setTextField() {
		this.textField.setText("");
	}

	public void setBtnConnect(JButton btnConnect) {
		this.btnConnect = btnConnect;
	}

}
