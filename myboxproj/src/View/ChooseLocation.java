package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;

public class ChooseLocation extends JDialog {

	JDialog window=new JDialog();
	JButton btnOk;
	private final JPanel panel = new JPanel();
	AddFileUI af;
	JTree tree= new JTree();
	PrivateSpaceUI gui;

	
	public ChooseLocation(PrivateSpaceUI gui, AddFileUI af) {
		setBounds(100, 100, 300, 350);
		this.af=af;
		this.gui=gui;
		setTitle("Choose file location");
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		ImageIcon icon =new ImageIcon(getClass().getResource("/images/icon.png"));
		setIconImage(icon.getImage());
		getContentPane().setLayout(new BorderLayout());
		panel.setBackground(new Color(250, 240, 230));
		panel.setLayout(null);
		getContentPane().add(panel);
		
		btnOk = new JButton("OK");
		btnOk.setBackground(new Color(245, 255, 250));
		btnOk.setBounds(225, 78, 59, 23);
		panel.add(btnOk);
		tree=gui.getGui().getTheTree();
		tree.setSize(180,270);
		tree.setLocation(20,20);
		tree.setVisible(true);
		panel.add(tree);	

		btnOk.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String press= gui.getGui().getFile().getPath();
				af.setLblLocation(press);
				dispose();
			}
		});
	
	
		

}	
}
