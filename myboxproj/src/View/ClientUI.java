package View;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;

import Communication.Client;
import Controller.ClientController;
import Controller.SystemMessageController;
import Entity.Message1;
import Entity.User;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.Serializable;

public class ClientUI extends JFrame implements Serializable {

	private JPanel contentPane;
	private JTextField Unametxt;
	private JPasswordField passtxt;
	JLabel lblWelcomeToMybox;
	JLabel lblUserName;
	JLabel lblUserPassword;
	private JButton btnSignIn;
	public JButton getBtnSignIn() {
		return btnSignIn;
	}

	public void setBtnSignIn(JButton btnSignIn) {
		this.btnSignIn = btnSignIn;
	}




	private String name;
	private String pass;
	User user;
	ClientController cc;
	public Client clien;
	private ConnectToServerUI cs;
	Message1 mm;

	
	public ClientController getCc() {
		return cc;
	}

	public void setCc(ClientController cc) {
		this.cc = cc;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	public Client getClien() {
		return clien;
	}

	public void setClien(Client clien) {
		this.clien = clien;
	}
	/**
	 * Create the frame.
	 */
	public ClientUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("MyBox Client");
		setBounds(100, 100, 500, 300);
		//setVisible(true);
		setResizable(false);
		ImageIcon icon =new ImageIcon(getClass().getResource("/images/icon.png"));
		setIconImage(icon.getImage());
		contentPane = new JPanel();
		contentPane.setBackground(new Color(250, 240, 230));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		lblWelcomeToMybox = new JLabel("Welcome to MyBox!", SwingConstants.CENTER);
		lblWelcomeToMybox.setFont(new Font("Tahoma", Font.BOLD, 36));
		lblWelcomeToMybox.setBounds(44, 11, 367, 48);
		contentPane.add(lblWelcomeToMybox);

		lblUserName = new JLabel("User name:");
		lblUserName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblUserName.setBounds(25, 109, 152, 19);
		contentPane.add(lblUserName);

		lblUserPassword = new JLabel("User Password:");
		lblUserPassword.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblUserPassword.setBounds(25, 159, 152, 19);
		contentPane.add(lblUserPassword);

		
		
		
		btnSignIn = new JButton("Sign In");
		btnSignIn.setBackground(new Color(245, 255, 250));
		btnSignIn.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSignIn.setBounds(184, 216, 98, 37);
		contentPane.add(btnSignIn);
		

		Unametxt = new JTextField();
		Unametxt.setBounds(254, 110, 180, 20);
		contentPane.add(Unametxt);
		Unametxt.setColumns(10);

		passtxt = new JPasswordField();
		passtxt.setColumns(10);
		passtxt.setBounds(254, 160, 180, 20);
		contentPane.add(passtxt);
		

	}

	public void clearFields()
	{
		Unametxt.setText("");
		passtxt.setText("");
	}


	public String getUnametxt() {
		String str = new String(Unametxt.getText());
		return str;
	}

	public void setUnametxt(JTextField Unametxt) {
		Unametxt.setText("");
	}

	public String getPasstxt() {
		String str = new String(passtxt.getPassword());
		return str;
	}

	public void setPasstxt(JPasswordField passtxt) {
		passtxt.setText("");
	}

	public void displayMsg (String m)
	{
		JOptionPane.showMessageDialog(this, m);
	}
	
	
	
	
	public void addLoginActionListener(ActionListener a)
	{
		btnSignIn.addActionListener(a);
	}
}
