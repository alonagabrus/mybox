package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JDesktopPane;
import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.tree.TreeModel;

import java.awt.Component;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JScrollBar;
import javax.swing.SwingConstants;
import javax.swing.JToggleButton;

import java.awt.ScrollPane;

import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JRadioButton;

import java.awt.Choice;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JComboBox;

import Controller.AddFileController;
import Controller.PrivateSpaceController;
import Controller.StartClientController;
import Entity.SysFile;
import Entity.User;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;



public class PrivateSpaceUI extends JFrame{

	JPanel contentPane;
	JTextField textField;
	public String search;
	public String remove_type;
	public int num;
	JButton btnNewFolder;
	JButton btnAddFile;
	JButton btnEditDetails;
	JButton btnRenameFila;
	JButton btnManagePermission;
	JComboBox remove;
	JButton btnNewButton ;
	ImageIcon srh;
	JList list;
	JButton logOut;
	JScrollPane scrollPane;
	JLabel lblFiles ;
	JButton btnUpdateFile;
	ArrayList<String> array;
	JTree theTree; 
	FileSystemModel filesysmodel;
	File file;
	
	public ArrayList<String> getArray() {
		return array;
	}

	public void setArray(ArrayList<String> fileList) {
		this.array = fileList;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrivateSpaceUI frame = new PrivateSpaceUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrivateSpaceUI() {                                                                                 

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Private Space");
		setBounds(0, 0, 600, 500);
		setResizable(false);
		ImageIcon icon =new ImageIcon(getClass().getResource("/images/icon.png"));
		setIconImage(icon.getImage());
		contentPane = new JPanel();

		contentPane.setBackground(new Color(250, 240, 230));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		btnAddFile = new JButton("Add File");
		btnAddFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddFileController newfile = new AddFileController(StartClientController.conClient);

			}
		});
		btnAddFile.setBackground(new Color(245, 255, 250));
		btnAddFile.setBounds(21, 10, 134, 23);
		contentPane.add(btnAddFile);
		btnNewFolder = new JButton("New Folder");
		btnNewFolder.setBackground(new Color(245, 255, 250));
		btnNewFolder.setEnabled(true);
		btnNewFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String foldername = JOptionPane.showInputDialog(btnNewFolder,"New Folder name", null);
				while(foldername.length()==0)
				{
					JOptionPane.showMessageDialog(btnNewFolder, "Folder name cannot be empty.Try again", "Oops..",0,null);
					foldername = JOptionPane.showInputDialog(btnNewFolder,"New Folder name", null);

				}
				
				File folder= new File(file.getPath()+"\\"+foldername);
				folder.mkdir();
				contentPane.add(theTree);
				
				
			}
	

			private void showMessageDialog(String string) {
				// TODO Auto-generated method stub

			}
		});
		btnNewFolder.setBounds(21, 148, 134, 23);
		contentPane.add(btnNewFolder);

		textField = new JTextField();
		textField.setBounds(407, 10, 134, 22);
		contentPane.add(textField);
		textField.setColumns(10);

		btnEditDetails = new JButton("Edit file details");
		btnEditDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditFileDetailsUI edit = new EditFileDetailsUI();
				edit.setVisible(true);
			}
		});
		btnEditDetails.setBackground(new Color(245, 255, 250));
		btnEditDetails.setEnabled(false);
		btnEditDetails.setBounds(21, 182, 134, 23);
		contentPane.add(btnEditDetails);

		btnRenameFila = new JButton("Rename file");
		btnRenameFila.setEnabled(false);
		btnRenameFila.setBackground(new Color(245, 255, 250));
		btnRenameFila.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String newname = JOptionPane.showInputDialog(btnRenameFila,
						"New file name", null);				
			}
		});

		btnRenameFila.setBounds(21, 114, 134, 23);
		contentPane.add(btnRenameFila);

		btnManagePermission = new JButton("Change file access");
		btnManagePermission.setBackground(new Color(245, 255, 250));
		btnManagePermission.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManagePermissionsUI window=new ManagePermissionsUI();
				window.setVisible(true);


			}
		});
		btnManagePermission.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnManagePermission.setBounds(21, 216, 134, 23);
		btnManagePermission.setEnabled(false);
		contentPane.add(btnManagePermission);


		remove = new JComboBox();		
		remove.setMaximumRowCount(3);
		remove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox res = (JComboBox) e.getSource();
				String selection = (String) res.getSelectedItem();
				if(selection.equals("Permanently"))
				{
					System.out.println(num);
				}

			}
		});

		remove.setEditable(true);
		remove.setBackground(new Color(245, 255, 250));
		remove.setEnabled(false);
		remove.addItem("Remove File");
		remove.addItem("Temporary");
		remove.addItem("Permanently");
		remove.setBounds(21, 80, 134, 23);


		contentPane.add(remove);

		btnNewButton = new JButton("");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				search = textField.getSelectedText().toLowerCase();
			}
		});
		btnNewButton.setBackground(new Color(245, 255, 250));
		srh =new ImageIcon(getClass().getResource("/images/search.png"));
		btnNewButton.setIcon(srh);
		btnNewButton.setBounds(545, 10, 28, 24);
		contentPane.add(btnNewButton);
		
		lblFiles = new JLabel("Files");
		lblFiles.setForeground(new Color(160, 82, 45));
		lblFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblFiles.setBounds(187, 21, 102, 23);
		contentPane.add(lblFiles);



		btnUpdateFile = new JButton("Update file");
		btnUpdateFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				UpdateFileUI filewin = new UpdateFileUI();
			}
		});
		btnUpdateFile.setBackground(new Color(245, 255, 250));
		btnUpdateFile.setBounds(21, 46, 134, 23);
		disable_update();
		contentPane.add(btnUpdateFile);

		logOut = new JButton("Log Out");
		logOut.setBounds(32, 412, 102, 36);
		contentPane.add(logOut);

		
		filesysmodel=new FileSystemModel(new File("C:\\Server\\"+User.Uname));
		theTree=new JTree(filesysmodel);
		theTree.setVisibleRowCount(15); 
		theTree.setVisible(true);
		theTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);  
		theTree.setLocation(187, 62);
		scrollPane = new JScrollPane(theTree);
		scrollPane.setSize(373, 386);
		scrollPane.setLocation(187, 62);
		contentPane.add(scrollPane);
		theTree.addTreeSelectionListener(new TreeSelectionListener() {
		      public void valueChanged(TreeSelectionEvent event) {
		        file = (File) theTree.getLastSelectedPathComponent();
		        System.out.println(file.getPath());
		      }
		    });
	}

	public void enable_remove ()
	{
		remove.setEnabled(true);
	}
	public void disable_remove ()
	{
		remove.setEnabled(false);
	}
	public void enable_update ()
	{
		btnUpdateFile.setEnabled(true);
	}
	public void disable_update ()
	{
		btnUpdateFile.setEnabled(false);
	}
	public void enable_rename ()
	{
		btnRenameFila.setEnabled(true);
	}
	public void disable_rename ()
	{
		btnRenameFila.setEnabled(false);
	}
	public void enable_edit ()
	{
		btnEditDetails.setEnabled(true);
	}
	public void disable_edit ()
	{
		btnEditDetails.setEnabled(false);
	}
	public String getTextField() {
		return textField.getSelectedText();
	}

	public void setTextField(JTextField textField) {
		this.textField.setText("");
	}
	public String getList() {
		return (String) list.getSelectedValue();
	}
	public void setlist(File[] newlist) {
		list = new JList<File>(newlist);
	}
	public void addSelectFileActionListener(ListSelectionListener listener)
	{
		list.addListSelectionListener(listener);
	}
	public void addSearchFileActionListener(ActionListener listener)
	{
		btnNewButton.addActionListener(listener);
	}
	public void addAddFileActionListener(ActionListener listener)
	{
		btnAddFile.addActionListener(listener);
	}
	public void addUpdateFileActionListener(ActionListener listener)
	{
		btnUpdateFile.addActionListener(listener);
	}
	public void addRenameFileActionListener(ActionListener listener)
	{
		btnRenameFila.addActionListener(listener);
	}
	public void addNewFolderActionListener(ActionListener listener)
	{
		btnNewFolder.addActionListener(listener);
	}
	public void addEditDetailsActionListener(ActionListener listener)
	{
		btnEditDetails.addActionListener(listener);
	}
	public void addChangeAccessActionListener(ActionListener listener)
	{
		btnManagePermission.addActionListener(listener);
	}

	public void addRemoveTypeActionListener(ActionListener listener)
	{
		remove.addActionListener(listener);
	}
	public void addLogoutActionListener(ActionListener listener)
	{
		logOut.addActionListener(listener);
	}

	

	public JTree getTheTree() {
		return theTree;
	}

	public void setTheTree(JTree theTree) {
		this.theTree = theTree;
	}

	public PrivateSpaceUI getGui()
	{return this;
	}
	
	
	class FileSystemModel implements TreeModel {
		  private File root;
		 
		  private Vector listeners = new Vector();
		 
		  public FileSystemModel(File rootDirectory) {
		    root = rootDirectory;
		  }
		 
		  public Object getRoot() {
		    return root;
		  }
		 
		  public Object getChild(Object parent, int index) {
		    File directory = (File) parent;
		    String[] children = directory.list();
		    return new TreeFile(directory, children[index]);
		  }
		 
		  public int getChildCount(Object parent) {
		    File file = (File) parent;
		    if (file.isDirectory()) {
		      String[] fileList = file.list();
		      if (fileList != null)
		        return file.list().length;
		    }
		    return 0;
		  }
		 
		  public boolean isLeaf(Object node) {
		    File file = (File) node;
		    return file.isFile();
		  }
		 
		  public int getIndexOfChild(Object parent, Object child) {
		    File directory = (File) parent;
		    File file = (File) child;
		    String[] children = directory.list();
		    for (int i = 0; i < children.length; i++) {
		      if (file.getName().equals(children[i])) {
		        return i;
		      }
		    }
		    return -1;
		 
		  }
		 
		  public void valueForPathChanged(TreePath path, Object value) {
		    File oldFile = (File) path.getLastPathComponent();
		    String fileParentPath = oldFile.getParent();
		    String newFileName = (String) value;
		    File targetFile = new File(fileParentPath, newFileName);
		    oldFile.renameTo(targetFile);
		    File parent = new File(fileParentPath);
		    int[] changedChildrenIndices = { getIndexOfChild(parent, targetFile) };
		    Object[] changedChildren = { targetFile };
		    fireTreeNodesChanged(path.getParentPath(), changedChildrenIndices, changedChildren);
		 
		  }
		 
		  private void fireTreeNodesChanged(TreePath parentPath, int[] indices, Object[] children) {
		    TreeModelEvent event = new TreeModelEvent(this, parentPath, indices, children);
		    Iterator iterator = listeners.iterator();
		    TreeModelListener listener = null;
		    while (iterator.hasNext()) {
		      listener = (TreeModelListener) iterator.next();
		      listener.treeNodesChanged(event);
		    }
		  }
		 
		  public void addTreeModelListener(TreeModelListener listener) {
		    listeners.add(listener);
		  }
		 
		  public void removeTreeModelListener(TreeModelListener listener) {
		    listeners.remove(listener);
		  }
		 
		  private class TreeFile extends File {
		    public TreeFile(File parent, String child) {
		      super(parent, child);
		    }
		 
		    public String toString() {
		      return getName();
		    }
		  }
		}


	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}
