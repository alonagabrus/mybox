package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PrivateSpaceUserUI extends PrivateSpaceUI {

	JPanel contentPane;
	JButton btnSendRequestTo;

	public static void main(String[] args) {
		try {
			PrivateSpaceUserUI dialog = new PrivateSpaceUserUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Create the frame.
	 */
	public PrivateSpaceUserUI() {
		super();
		setVisible(true);
		super.setTitle("Private space of user");		
		btnSendRequestTo = new JButton("Send Request");
		btnSendRequestTo.setBackground(new Color(245, 255, 250));	
		btnSendRequestTo.setBounds(22, 254, 134, 23);
		getContentPane().add(btnSendRequestTo);
		btnSendRequestTo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				RequestFormUI f= new RequestFormUI();
			}
		});
	}
	/*public void addSelectFileActionListener(ListSelectionListener listener)
	{
		list.addListSelectionListener(listener);
	}
	public void addSearchFileActionListener(ActionListener listener)
	{
		btnNewButton.addActionListener(listener);
	}
	public void addAddFileActionListener(ActionListener listener)
	{
		btnAddFile.addActionListener(listener);
	}
	public void addUpdateFileActionListener(ActionListener listener)
	{
		btnUpdateFile.addActionListener(listener);
	}
	public void addRenameFileActionListener(ActionListener listener)
	{
		btnRenameFila.addActionListener(listener);
	}
	public void addNewFolderActionListener(ActionListener listener)
	{
		btnNewFolder.addActionListener(listener);
	}
	public void addEditDetailsActionListener(ActionListener listener)
	{
		btnEditDetails.addActionListener(listener);
	}
	public void addChangeAccessActionListener(ActionListener listener)
	{
		btnManagePermission.addActionListener(listener);
	}
	public void addLogOutActionListener(ActionListener listener)
	{
		logOut.addActionListener(listener);
	}
	public void addRemoveTypeActionListener(ActionListener listener)
	{
		remove.addActionListener(listener);
	}*/
}
