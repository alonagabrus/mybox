package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class AddFileUI extends JDialog {

	private final JPanel contentPanel = new JPanel();
	JLabel lblChooseAFile;
	JTextArea textArea ;
	JLabel lblEnterAFile_1;
	JLabel lblmustBelessThan;
	JButton btnApply;
	JButton btnCancel;
	JButton btnBrowse;
	JLabel lblFile;
	JLabel lblLocation;
	JPanel panel=new JPanel();
	PrivateSpaceUI gui=new PrivateSpaceUI() ;
	JDialog window;
	JButton OK;
	
	File f1;
	public File getF1() {
		return f1;
	}
	public void setF1(File f1) {
		this.f1 = f1;
	}
	/**
	 * Create the dialog.
	 */
	public AddFileUI() {
		setBounds(100, 100, 450, 300);
		setTitle("Add a file");
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		ImageIcon icon =new ImageIcon(getClass().getResource("/images/icon.png"));
		setIconImage(icon.getImage());
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(250, 240, 230));
		contentPanel.setLayout(null);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			}
		});
		btnBrowse.setBackground(new Color(245, 255, 250));
		btnBrowse.setBounds(345, 13, 89, 23);
		contentPanel.add(btnBrowse);
		
		lblChooseAFile = new JLabel("Choose a file:");
		lblChooseAFile.setBounds(10, 15, 101, 19);
		contentPanel.add(lblChooseAFile);
		{
			JLabel lblEnterAFile = new JLabel("Insert a file location:");
			lblEnterAFile.setBounds(10, 45, 145, 23);
			contentPanel.add(lblEnterAFile);
		}
		
		textArea = new JTextArea();
		textArea.setBounds(165, 108, 222, 99);
		contentPanel.add(textArea);
		
		lblEnterAFile_1 = new JLabel("Insert a file description:");
		lblEnterAFile_1.setBounds(10, 118, 145, 19);
		contentPanel.add(lblEnterAFile_1);
		
		lblmustBelessThan = new JLabel("(must be less than 30 words)");
		lblmustBelessThan.setFont(new Font("Times New Roman", Font.PLAIN, 10));
		lblmustBelessThan.setBounds(10, 132, 145, 23);
		contentPanel.add(lblmustBelessThan);
		
		btnApply = new JButton("Apply");
		btnApply.setBackground(new Color(245, 255, 250));
		btnApply.setBounds(96, 227, 89, 23);
		contentPanel.add(btnApply);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBackground(new Color(245, 255, 250));
		btnCancel.setBounds(222, 227, 89, 23);
		contentPanel.add(btnCancel);
		
		lblFile = new JLabel("");
		lblFile.setBounds(165, 22, 146, 14);
		lblFile.setVisible(true);
		
		contentPanel.add(lblFile);
		
		JButton btnChooseLocation = new JButton("Choose location");
		
		btnChooseLocation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ChooseLocation cl=new ChooseLocation(gui,AddFileUI.this);
				
				
			}
		});
		
		
		
		btnChooseLocation.setBackground(new Color(245, 255, 250));
		btnChooseLocation.setBounds(144, 45, 145, 23);
		contentPanel.add(btnChooseLocation);
		
		lblLocation = new JLabel("");
		lblLocation.setBounds(139, 83, 268, 14);
		contentPanel.add(lblLocation);
	}
	
	
	public String getLblFile() {
		String str = new String(lblFile.getText());
		return str;
	}
	public void setLblFile(String s) {
		this.lblFile.setText(s);
	}

	public String getTextArea() {
		String str = new String(textArea.getText());
		return str;
	}
	public void setTextArea(JTextArea textArea) {
		this.textArea.setText("");
	}
	public JButton getBtnApply() {
		return btnApply;
	}
	public JButton getBtnCancel() {
		return btnCancel;
	}
	
	
	/****Action Listeners*///
	
	public void addApplylActionListener(ActionListener listener)
	{
		btnApply.addActionListener(listener);
	}
	public void addBrouseActionListener(ActionListener listener)
	{
		btnBrowse.addActionListener(listener);
	}
	public String getLblLocation() {
		String str = new String(lblLocation.getText());
		return str;
	}
	public void setLblLocation(String s) {
		this.lblLocation.setText(s);
	}
}
