package View;

import java.awt.*;

import javax.swing.*;

import Communication.EchoServer;
import Controller.SystemMessageController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ServerUI extends JFrame {

	JPanel contentPane;
	JLabel DBname;
	JLabel portnum;
	JLabel DBpass;
	JLabel DBuser;
	private JTextField nametext;
	private JTextField usertxt;
	private JTextField porttxt;
	private JPasswordField passtxt;
	JButton connectbtn;
	private JTextField textField;
	JLabel lblConnectionStatus;
	ServerUI server;
	private String scheme;
	private String username;
	private int p;
	private String port;
	private String password;
	EchoServer Eserver;
	private LogViewUI lv;
	private Connection conn;
	ConnectToServerUI conser;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServerUI serverui = new ServerUI();
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public void setServer(ServerUI server)
	{
		this.server = server;
	}

	/**
	 * Create the frame.
	 */
	public ServerUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 400);
		setVisible(true);
		setTitle("MyBox Server");
		setResizable(false);
		ImageIcon icon =new ImageIcon(getClass().getResource("/images/icon.png"));
		setIconImage(icon.getImage());
		contentPane = new JPanel();
		contentPane.setBackground(new Color(250, 240, 230));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		DBname = new JLabel("Data Base name:");
		DBname.setFont(new Font("Tahoma", Font.BOLD, 15));
		DBname.setBounds(24, 44, 167, 20);
		contentPane.add(DBname);

		DBuser = new JLabel("Data Base User:");
		DBuser.setFont(new Font("Tahoma", Font.BOLD, 15));
		DBuser.setBounds(24, 87, 167, 20);
		contentPane.add(DBuser);

		portnum = new JLabel("Port Number:");
		portnum.setFont(new Font("Tahoma", Font.BOLD, 15));
		portnum.setBounds(24, 178, 167, 20);
		contentPane.add(portnum);

		DBpass = new JLabel("Data Base Password:");
		DBpass.setFont(new Font("Tahoma", Font.BOLD, 15));
		DBpass.setBounds(24,132, 167, 20);
		contentPane.add(DBpass);

		nametext = new JTextField();
		nametext.setColumns(10);
		nametext.setText("mybox");
		nametext.setBounds(229, 44, 180, 20);
		contentPane.add(nametext);

		usertxt = new JTextField();
		usertxt.setColumns(10);
		usertxt.setText("root");
		usertxt.setBounds(229, 89, 180, 20);
		contentPane.add(usertxt);

		porttxt = new JTextField();
		porttxt.setColumns(10);
		porttxt.setText("5555");
		porttxt.setBounds(229, 180, 180, 20);
		contentPane.add(porttxt);

		passtxt = new JPasswordField();
		passtxt.setText("12345");
		passtxt.setColumns(10);
		passtxt.setBounds(229, 134, 180, 20);
		contentPane.add(passtxt);

		connectbtn = new JButton("Connect");
		connectbtn.setBackground(new Color(245, 255, 250));
		connectbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (nametext.getText().equals("")|| usertxt.getText().equals("")|| porttxt.getText().equals("")|| passtxt.getPassword().equals(""))
				{
					displayWarningMessage("Please fill all the fields");
				}
				else
				{
					setScheme(getNametext());
					setUsername(getUsertxt());
					setPort(getPorttxt());
					setPassword(getPasstxt());
					p=Integer.parseInt(port);
					
					if(openConnectionDB()){
						Eserver=new EchoServer(p);
						Eserver.setConn(conn);

						try 
						{
							Eserver.listen(); //Start listening for connections
							dispose();
							

						} 
						catch (Exception ex) 
						{
							ex.printStackTrace();
							SystemMessageController.displayErrorMessage("ERROR - Could not listen for clients!");
							
						}
					
				}
			}
		}});
		connectbtn.setBounds(175, 209, 100, 39);
		connectbtn.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(connectbtn);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBackground(new Color(250, 240, 230));
		textField.setBounds(24, 284, 429, 76);
		contentPane.add(textField);
		textField.setColumns(10);

		lblConnectionStatus = new JLabel("Connection status:");
		lblConnectionStatus.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblConnectionStatus.setBounds(172, 259, 117, 14);
		contentPane.add(lblConnectionStatus);

	}
	public int getP() {
		return p;
	}
	public void setP(int p) {
		this.p = p;
	}
	public void displayWarningMessage(String msg)
	{
		JOptionPane.showMessageDialog(this, msg);
	}
	/***Setters and Getters*/
	public String getNametext() {
		return nametext.getText();
	}
	public String getUsertxt() {
		return usertxt.getText();
	}
	public String getPorttxt() {
		return porttxt.getText();
	}
	public String getPasstxt() {
		return String.valueOf(passtxt.getPassword());
	}
	public String getTextField() {
		return textField.getText();
	}
	public void setNametext() {
		this.nametext.setText("");
	}
	public void setUsertxt() {
		this.usertxt.setText("");
	}
	public void setPorttxt() {
		this.porttxt.setText("");
	}
	public void setPasstxt() {
		this.passtxt.setText("");
	}
	public void setTextField(String text) {
		this.textField.setText(text);
	}
	public String getScheme() {
		return scheme;
	}
	public String getUsername() {
		return username;
	}
	public String getPort() {
		return port;
	}
	public String getPassword() {
		return password;
	}
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	/****ActionListeners*/
	public void addConnectActionListener(ActionListener listener)
	{
		connectbtn.addActionListener( listener);
	}
	
	public boolean openConnectionDB(){
		try 
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (Exception ex) {}

		try 
		{
			
			//conn = DriverManager.getConnection("jdbc:mysql://localhost/mybox",username,password);
			conn = DriverManager.getConnection("jdbc:mysql://localhost/"+scheme+"",username,password);
			setTextField("SQL connection succeed");
			lv= new LogViewUI();
			lv.setVisible(true);
			return true;
			

		} catch (SQLException ex) 
		{
			setTextField("SQLException: " + ex.getMessage());
			setTextField("SQLState: " + ex.getSQLState());
			setTextField("VendorError: " + ex.getErrorCode());
			return false;
		}

	}
}
