package Entity;
import java.io.Serializable;
import java.util.List;

import Communication.Client;

public class User implements Serializable {
	public static String Uname;
	private String UserName;
	private String Password;
	private String FullName;
	private String Status = "0";
	private String isSysAdmin= "0";
	
	
	public User()
	{
		
	}
	public User(String UN, String P)
	{
		setUserName(UN);
		setPassword(P);
	}
	public User(String UN, String P,String st, String FN, String sa) {
		setUserName(UN);
		setPassword(P);
		setFullName(FN);
		isSysAdmin=sa;
	}
	public String getUserName()
	{
		return this.UserName;
	}
	
	public String getPassword() {
		return Password;
	}
	public String getFullName() {
		return FullName;
	}
	
	public void setUserName(String userName) {
		UserName = userName;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public void setFullName(String fullName) {
		FullName = fullName;
	}
	
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getIsSysAdmin() {
		return isSysAdmin;
	}
	public void setIsSysAdmin(String isSysAdmin) {
		this.isSysAdmin = isSysAdmin;
	}
	
	
	}

