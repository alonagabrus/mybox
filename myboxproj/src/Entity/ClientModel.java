package Entity;

public class ClientModel {
	private String host;
	private int port;
	
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = new String(host);
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	

}
