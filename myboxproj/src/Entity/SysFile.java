package Entity;
import java.io.File;
import java.io.Serializable;

public class SysFile  implements Serializable {

	private String File_Name;
	private String Description;
	private String Location;
	private String File_Status = "0";
	private String Access_Type = "3";
	private File f;
	private String FileOwner;
	public byte[] byteArray;
	private int size=0;
	

	public void initArray(int size)
	{
		byteArray= new byte[size];
	}
	
	public byte[] getByteArray() {
		return byteArray;
	}

	public byte getByteArray(int i) {
		return byteArray[i];
	}


	public void setByteArray(byte[] byteArray) {
		for(int i=0;i<byteArray.length;i++)
			this.byteArray[i]= byteArray[i];
	}


	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	public String getFileOwner() {
		return FileOwner;
	}


	public void setFileOwner(String fileOwner) {
		FileOwner = fileOwner;
	}


	public File getF() {
		return f;
	}

	public void setF(File f) {
		this.f = f;
	}

	public String getFile_name() {
		return File_Name;
	}

	public void setFile_name(String File_Name) {
		this.File_Name=File_Name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description=Description;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String Location) {
		this.Location=Location;
	}

	public String getFile_Status() {
		return File_Status;
	}

	public void setFile_Status(String File_Status) {
		this.File_Status=File_Status;
	}

	public String getAccess_Type() {
		return Access_Type;
	}

	public void setAccess_Type(String Access_Type) {
		this.Access_Type=Access_Type;
	}

	public SysFile(String name, String des, String loc, String fs, String atype, String owner) {
		File_Name=name;
		Description=des;
		Location=loc;
		File_Status=fs;
		Access_Type=atype;
		FileOwner=owner;
	
	}
	public SysFile(){}

}