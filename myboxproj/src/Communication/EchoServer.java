package Communication;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.nio.file.*;
import java.sql.*;

import com.mysql.jdbc.PreparedStatement;

import java.nio.file.*;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import View.ClientUI;
import View.LogViewUI;
import View.PrivateSpaceSysAdminUI;
import View.PrivateSpaceUserUI;
import View.PrivateSpaceUI;
import Controller.StartClientController;
import Controller.SystemMessageController;
import Entity.Message1;
import Entity.SysFile;
import Entity.User;



/**
 * This class overrides some of the methods in the abstract 
 * superclass in order to give more functionality to the server.
 *
 * @author Dr Timothy C. Lethbridge
 * @author Dr Robert Lagani&egrave;re
 * @author Fran&ccedil;ois B&eacute;langer
 * @author Paul Holden
 * @version July 2000
 */

public class EchoServer extends AbstractServer 
{
	//Class variables *************************************************
	/**
	 * The default port to listen on.
	 */

	private Connection conn;
	public LogViewUI v=new LogViewUI();
	private Statement stmt;
	User user=null;
	PrivateSpaceUI psui=new PrivateSpaceUI();


	//Constructors ****************************************************

	public EchoServer(int port) 
	{
		super(port);
	}

	//Instance methods ************************************************

	/**
	 * This method handles any messages received from the client.
	 *
	 * @param msg The message received from the client.
	 * @param client The connection from which the message originated.
	 */

	public void handleMessageFromClient  (Object msg, ConnectionToClient client)
	{		
		ResultSet rs=null;
		SysFile f=null;
		v.setTextArea("Message received: " + msg + " from " + client);

		Message1 temp= (Message1) msg;

		try {
			stmt = conn.createStatement();

			/**************************** LOG IN ********************************/
			if(temp.getTask().equals("searchLogin"))
			{
				int flag=0;
				String r="SELECT * FROM user WHERE User_Name = '"+((User)temp.getObject()).getUserName()+"'";
				rs = stmt.executeQuery(r);
				while(rs.next())
				{
					flag++;
					user=new User(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5));
				}
				if (flag==0)			//user is not exists in DataBAse
					client.sendToClient("Not found User");
				else					//user is in DataBAse + check password
				{
					rs=null;
					flag=0;
					String r2="SELECT password FROM user WHERE User_Name = '"+((User)temp.getObject()).getUserName()+"' AND password= '"+((User)temp.getObject()).getPassword()+"'";
					rs=stmt.executeQuery(r2);
					while(rs.next())
						flag++;
					if (flag==0)	//password not correct
						client.sendToClient("The password is incorrect");

					else					//password  correct + check if the user already login
					{	
						rs=null;
						flag=0;
						String r3="SELECT * FROM user WHERE User_Name = '"+((User)temp.getObject()).getUserName()+"' AND User_Status= '1'";
						rs = stmt.executeQuery(r3);
						while(rs.next())
							flag++;
						if (flag!=0)		//if status=1 is already loggin
							client.sendToClient("The user is already log in");

						else									//if status=0 is not loggin
						{

							String name =((User)temp.getObject()).getUserName();
							String r4="UPDATE user SET User_Status= '1' WHERE User_Name = '"+name+"'";	//update: status=1
							stmt.executeUpdate(r4);
							
							
							client.sendToClient(user);		//the user can connect to system
						}
					}

				}
			}

		}catch (Exception e) {
			e.printStackTrace();}

		/**************************** ADD FILE ********************************/
		if(temp.getTask().equals("checkFileName"))
		{
			try {
				stmt = conn.createStatement();
				int flag=0;
				String str=(String)temp.getObject();
				String r="SELECT * FROM file WHERE File_Name = '"+str+"'";
				rs = stmt.executeQuery(r);
				while(rs.next())
					flag++;

				if (flag==0)			//file name not exist in DB
				{
					//f=new SysFile(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),user.getUserName());
					client.sendToClient("file name was not found");

				}
				else 
					client.sendToClient("file name was found");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(temp.getTask().equals("save"))			//save file on server computer
		{
			
			try {
				FileOutputStream fos= new FileOutputStream(new File(psui.getFile().getPath()));
				fos.write((byte[])temp.getObject());
				fos.close();
				client.sendToClient("file was saved");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
		if(temp.getTask().equals("logout"))
		{
			String upd = "UPDATE user SET User_Status= '0' WHERE User_Name = '"+((User)temp.getObject()).getUserName()+"'";
			try {
				stmt.executeUpdate(upd);
				
				client.sendToClient("success");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if(temp.getTask().equals("updatefile"))
		{
			
			String fname=((SysFile)temp.getObject()).getFile_name();
			String des= ((SysFile)temp.getObject()).getDescription();
			String loc=((SysFile)temp.getObject()).getLocation();
			String status=((SysFile)temp.getObject()).getFile_Status();
			String access=((SysFile)temp.getObject()).getAccess_Type();
			String owner = temp.getMess();
			

			String u1= "INSERT INTO mybox.file VALUES('"+fname+"','"+des+"','"+loc+"','"+status+"','"+access+"','"+owner+"' )";
			//String u2= "INSERT INTO mybox.users_of_file VALUES('"+fname+"','"+owner+"','all_users' )";	//insert to table ,the file must be presented to him
			try {
				stmt.executeUpdate(u1);
			//	stmt.executeUpdate(u2);
				client.sendToClient("doneUpdate");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
		}


	}

	/**
	 * This method overrides the one in the superclass.  Called
	 * when the server starts listening for connections.
	 */
	public Connection getConn() {
		return conn;}

	public void setConn(Connection conn) {
		this.conn = conn;
	}


	protected void serverStarted()
	{
		System.out.println
		("Server listening for connections on port " + getPort());
	}

	/**
	 * This method overrides the one in the superclass.  Called
	 * when the server stops listening for connections.
	 */
	protected void serverStopped()
	{
		System.out.println
		("Server has stopped listening for connections.");
	}

	//Class methods ***************************************************

	/**
	 * This method is responsible for the creation of 
	 * the server instance (there is no UI in this phase).
	 *
	 * @param args[0] The port number to listen on.  Defaults to 5555 
	 *          if no argument is entered.
	 */






}

//End of EchoServer class

