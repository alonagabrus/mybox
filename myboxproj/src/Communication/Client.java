package Communication;



import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import Controller.AddFileController;
import Controller.ClientController;
import Controller.PrivateSpaceController;
import Controller.SystemMessageController;
import Entity.SysFile;
import Entity.User;

public class Client extends ObservableClient{


	ArrayList<String> groupList;
	private Object currController;


	public void setCurrObj(Object c)
	{
		this.currController=c;
	}



	public Object getCurrController() {
		return currController;
	}



	public void setCurrController(Object currController) {
		this.currController = currController;
	}



	public Client(String host, int port) throws IOException {
		super(host, port);
		
		groupList= new ArrayList<String>();
		openConnection();


	}



	public void quit() {
		try {
			closeConnection();
		} catch (IOException e) {
		}
		System.exit(0);
	}

	public void handleMessageFromClientUI(Object message)
	{
		try
		{
			sendToServer(message);
		}
		catch(IOException e)
		{
			SystemMessageController.displayMessage("Could not send message to server.  Terminating client.");
			quit();
		}
	}



	@Override
	public synchronized void handleMessageFromServer(Object msg) {

		if(msg instanceof User)
		{
			try{
				((ClientController)currController).handleDBResult(msg);
			}
			catch(Exception e)
			{e.printStackTrace();};
		}
		if(msg instanceof SysFile)
		{
			try{
				((AddFileController)currController).handleDBResult(msg);
			}
			catch(Exception e)
			{e.printStackTrace();};
		}
		if (msg instanceof String)
		{
			if(((String)msg).equals("Not found User") )
			{
				SystemMessageController.displayMessage("This user is not exists in DataBase !");
			}
			if(((String)msg).equals("The password is incorrect") )
			{

				SystemMessageController.displayMessage("The password is incorrect");
			}
			if(((String)msg).equals("The user is already log in") )
			{
				SystemMessageController.displayMessage("This user is already Login");
			}
			if(((String)msg).equals("file name was found") )
			{
				SystemMessageController.displayMessage("This file name is already exist in DataBase");
			}
			if(((String)msg).equals("file name was not found") )
			{
				try{
					System.out.println("blaaa");
					((AddFileController)currController).handleDBResult("");
				}
				catch(Exception e)
				{e.printStackTrace();};
			}
			if(((String)msg).equals("file was saved") )
			{
				SystemMessageController.displayMessage("This file was saved in server");
				
			}

			if(((String)msg).equals("success") )
			{
				((PrivateSpaceController)currController).handleDBResult("logout");
			}
			if(((String)msg).equals("doneUpdate") )
			{
				((AddFileController)currController).handleDBResult("doneUpdate");
			}
		}


	}


}



