package Communication;

import java.util.ArrayList;

public class MessageHandle extends ArrayList  {

	/**
	 * 
	 */
	public static final int USER = 1;
	public static final int WORKER_STATUS = 2;
	public static final int INSERT_PRODUCT = 3;


	private Object data;
	private int type;
	private boolean updateDB;
	private String msg;
	/**
	 * 
	 * @return  msg
	 */
	public String getMsg() {
		return msg;
	}
/**
 * 
 * @param msg
 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
/**
 * Handle the given message.
 * @param data- the object to be handled
 * @param type- type of the message to be handled
 * @param updateDB
 */
	public MessageHandle(Object data, int type, boolean updateDB) {
		this.data = data;
		this.type = type;
		this.updateDB = updateDB;
	}
	/**
	 * Handle the given message.
	 * @param data
	 * @param type
	 * @param updateDB
	 */
	public MessageHandle(String data, int type, boolean updateDB) {
		this.data = data;
		this.type = type;
		this.updateDB = updateDB;
	}
	/**
	 * 
	 * @return data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * get the type of the Message (Each message is number)
	 * @return type
	 */
	public int getType() {
		return type;
	}
	/**
	 * 
	 * @return updateDB
	 */
	public boolean isUpdateDB() {
		return updateDB;
	}
	/**
	 * 
	 * @param data -the object we handle with
	 */
	public void setData(Object data) {
		this.data = data;
	}
	/**
	 * 
	 * @param type -number of the type Message
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * 
	 * @param updateDB (if its true =update the database if false= don't update the database)
	 */
	public void setUpdateDB(boolean updateDB) {
		this.updateDB = updateDB;
	}
}


